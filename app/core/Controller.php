<?php

spl_autoload_register(function ($class) {
    $path = '../app/models/DbAccess/' . ucfirst($class) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
    $path2 = '../app/models/' . ucfirst($class) . '.php';
    if (file_exists($path2)) {
        require_once $path2;
    }
    $path3 = '../app/models/Service/' . ucfirst($class) . '.php';
    if (file_exists($path3)) {
        require_once $path3;
    }

});

require_once ('../app/vendor/autoload.php');

/**
 * The controller class.
 *
 * The base controller for all other controllers. Extend this for each
 * created controller and get access to it's wonderful functionality.
 */
class Controller
{
    /**
     * Render a view
     *
     * @param string $viewName The name of the view to include
     * @param array  $data Any data that needs to be available within the view
     *
     * @return void
     */
    public function view($viewName, $data = [])
    {
		require_once '../app/views/'.$viewName.'.phtml';
    }

    /**
     * Load a model
     *
     * @param string $model The name of the model to load
     *
     * @return object
     */
    public function model($model)
    {
        require_once '../app/models/' . ucfirst($model) . '.php';

        return new $model();
    }

    /**
     * Retrieves GET and POST variables from the request
     *
     * @param string $name the parameter we want to retrieve the value of from the request
     * @param string $method (optional) Specifies the exact method to use. If null, check for POST then GET
     *
     * @return string value of given parameter
     */
    public function param($name, $method = null)
    {
        if(is_null($method))
        {
            if(isset($_POST[$name]))
            {
                return $_POST[$name];
            } else if(isset($_GET[$name]))
            {
                return $_GET[$name];
            }
        } else
        {
            if(strtoupper($method) == "POST")
            {
                return $_POST['$name'];
            } else if(strtoupper($method) == "GET")
            {
                return $_GET['$name'];
            }
        }
    }

    /**
     * Find the requesters IP address
     *
     * @return string IP address
     */
    public function requestIP()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}