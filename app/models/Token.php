<?php
/*
sample model 'Token'
requires a users_tokens table with the following schema:

CREATE TABLE IF NOT EXISTS `users_tokens` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `tokenHash` varchar(32) NOT NULL,
  `expires` int(11) NOT NULL COMMENT 'non-expiring tokens are set to zero',
  `type` varchar(30) NOT NULL DEFAULT 'system' COMMENT 'either -system- or -api-',
  `created` int(11) NOT NULL,
  `lastUsed` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

*/

class Token
{
  private $DB;
  private $token;
  private $hash;
  
  public function __construct($DB, $token = ''){
    $this->DB = $DB;
    if($token != ''){
      $this->token = $token;
      $this->hash = $this->hashToken($token);
    }
  }
  
  public function setToken($token){
    if($token != ''){
      $this->token = $token;
      $this->hash = $this->hashToken($token);
    }
  }
  
  private function hashToken($token){
    return md5($token);
  }
  
  public function getEmailFromToken(){
    
    $returnArray = array();
    if($this->token != ''){
      $currentTime = time();
      $userMatch = $this->DB->query("SELECT email, expires FROM users_tokens WHERE tokenHash = ? AND (expires > ? OR expires = '0')LIMIT 1", array($this->hash, $currentTime));
      if($userMatch->numRows() > 0){
        foreach($userMatch->fetchAll() as $user){
          $returnArray['token'] = $this->token;
          $returnArray['email'] = $user['email'];
          $returnArray['expires'] = $user['expires'];
          //update last used
          $this->DB->query("UPDATE users_tokens SET lastUsed = ? WHERE tokenHash = ? LIMIT 1",array($currentTime, $this->hash));
        }
      } else{
        $returnArray['Error'] = 'Not a valid token';
      }
      
      
    } else{
      $returnArray['Error'] = 'No token provided';
    }
    return $returnArray;
    
  }
  
  public function generateNewToken($email = '', $password = '',$type = 'api'){
    
    if($email != '' && $password != ''){
      $returnArray = array();
      
      //check if username and password match an account
      $userMatch = $this->DB->query("SELECT * FROM users WHERE email = ? AND password = ? AND emailConfirmed = 'yes'", array($email, md5($password)))->numRows();
      if($userMatch != 0){
        $newToken = bin2hex(random_bytes(20));
        $newHash = $this->hashToken($newToken);
        $time = time();
        $expires = $time + (86400 * 30 * 12);
        $this->DB->query("INSERT INTO users_tokens(email, tokenHash, expires, type, created, lastUsed) VALUES (?, ?, ?, ?, ?, ?)", array($email, $newHash, $expires, $type, $time, $time));
        
        $returnArray['email'] = $email;
        $returnArray['token'] = $newToken;
        $returnArray['expires'] = $expires;
        
      } else{
        $returnArray['Error'] = 'Invalid Email or Password';
        
      }
      return $returnArray;
    }
    
  }
  
}