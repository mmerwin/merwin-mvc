<?php

/**
 * The DataModels class.
 *
 * The base data class for database access. Extend this for each
 * created DbAccess model.
 */
class DataModel extends DB
{
    protected $DB;
    protected $columns;
    protected $primaryKey;
    protected $lastQuery;

    public function __construct()
    {
        $this->DB = new DB();
        $this->describeTable();
    }

    protected function describeTable()
    {
        $this->columns = $this->DB->query("DESCRIBE {$this->schema}")->fetchAll();
        foreach($this->columns as $row){
            if($row['Key'] == "PRI"){
                $this->primaryKey = $row['Field'];
            }
        }
    }


    protected function deleteRow($primaryKey)
    {
        $sql = "DELETE FROM {$this->schema} WHERE {$this->primaryKey} = ? LIMIT 1";
        $params = array($primaryKey);
        $this->DB->query($sql, $params);
        $this->lastQuery = array("sql"=>$sql, "params"=>$params);
    }

    protected function deleteRowOnAttribute($attribute, $value, $limit = 1)
    {
        if($this->validateAttribute($attribute))
        {
            $sql = "DELETE FROM {$this->schema} WHERE {$attribute} = ? LIMIT {$limit}";
            $params = array($value);
            $this->lastQuery = array("sql"=>$sql, "params"=>$params);
            $this->DB->query($sql, $params);
        }
    }

    private function validateAttribute($attribute)
    {
        foreach ($this->columns as $row)
        {
            if($row['Field'] == $attribute)
            {
                return true;
            }
        }
        return false;
    }

    public function retrieveLastQuery($bind = false){
        if($bind)
        {
            $sql = $this->lastQuery['sql'];
            $params = $this->lastQuery['params'];
            foreach($params as $value)
                $sql = preg_replace ( '[\?]' , "'" . $value . "'" , $sql, 1 );
            return $sql;
        }
        else
        {
            return $this->lastQuery;
        }

    }


}