<?php

//this script is to be called after git-pull is successful
//If different scripts need to be pulled on test / staging / production, use the function getDomain to
//determine which environment is being used.

//verify script is only used on cli
(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && die('cli only');
include("app/models/DB.php");

function getDomain()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'] . '/';
    return $protocol . $domainName;
}

//run data data/deltas
$dir = dirname(__FILE__) . '/app/data/deltas';
$files1 = scandir($dir);
//print_r($files1);
$db = new DB();
for ($x = 2; $x < count($files1); $x++) {
    $commands = file_get_contents($dir . '/' . $files1[$x]);
    $db->query($commands);
    echo "executed {$files1[$x]} \n";
}
echo "end of execution";

?>