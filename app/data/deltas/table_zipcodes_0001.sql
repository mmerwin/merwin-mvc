CREATE TABLE IF NOT EXISTS `zipcodes` (
                                          `zip` int(11) NOT NULL PRIMARY KEY,
                                          `city` text NOT NULL,
                                          `state_id` varchar(5) NOT NULL,
                                          `state_name` varchar(20) NOT NULL,
                                          KEY `state_id` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;