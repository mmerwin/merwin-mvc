<?php

/**
 * The default home controller, called when no controller/method has been passed
 * to the application.
 */
class Dashboard extends Controller
{
    /**
     * The default controller method.
     *
     * @return void
     */
    public function index($framework = 'inspinia', $name = 'bravo')
    {
        
        
        $this->view('dashboards/'.$framework, [
            'name2' => $_SESSION['name2']
        ]);
    }
}

?>