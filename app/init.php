<?php

spl_autoload_register(function ($class) {
    include '../app/models/' . ucfirst($class) . '.php';
});

require_once '../app/core/App.php';
require_once '../app/core/Controller.php';
require_once '../app/core/View.php';
require_once ('../app/vendor/autoload.php');

?>
