<?php

/**
 * The default home controller, called when no controller/method has been passed
 * to the application.
 */
class Home extends Controller
{
    /**
     * The default controller method.
     *
     * @return void
     */
    public function index($name = 'alex', $mood = 'normal')
    {
      
        $user = $this->model('user');
        $user->name = $name;

        $this->view('home/index', [
            'name' => $user->name,
            'mood' => $mood,
            'name2' =>$_SESSION['name2']
        ]);
    }
  
  public function setsession($name = 'alex')
    {
      $_SESSION['name2'] = $name;
        $user = $this->model('user');
        $user->name = $name;

        $this->view('home/index', [
            'name' => $user->name,
            'mood' => $mood,
            'name2' =>$_SESSION['name2']
        ]);
    }
}

?>