<?php
/*
Sample controller
*/
require_once 'app/models/DB.php';
require_once 'app/models/Token.php';

class Authenticate extends Controller
{
  
  public function index()
  {
    $result = array("endpoint is found, method is not found");
   echo json_encode($result);  
  }  
  
  public function generateToken($email, $password, $type = 'api'){
    //takes in email address and password, and returns a new token. 
    $tokenModel = new Token($this->DB(), "token");
    echo json_encode($tokenModel->generateNewToken($email, $password, $type));
  }
  
  public function getEmailFromToken($token){
    //returns email associated with a valid non-expired token
    $tokenModel = new Token($this->DB(), $token);
    echo json_encode($tokenModel->getEmailFromToken());
  }
  
  public function verifyToken($token){
    return $this->getEmailFromToken($token);
  }
}