<?php

class DB_Zipcodes extends DataModel
{
    protected $schema = "zipcodes";

    public function validateZipcode($zip, $state_id){
        if($this->DB->query("SELECT * FROM {$this->schema} WHERE zip = ? AND state_id = ? LIMIT 1",array($zip, $state_id))->numRows() == 1){
            return true;
        } else{
            return false;
        }
    }

    public function addZipcode($zip, $city, $state_id){
        if(is_null($zip) || is_null($city) || is_null($state_id) || (self::validateState($state_id) == false) || !self::zipexists($zip)){
            return false;
        }
        $state_name = self::getStateFromStateId($state_id);
        if($state_name == "Invalid"){
            return false;
        }
        $this->DB->query("INSERT INTO {$this->schema}(zip, city, state_id, state_name) VALUES (?, ?, ?, ?)", array($zip, $city, $state_id, $state_name));

    }

    public function validateState($state_id){
        if(is_null($state_id)){
            return false;
        }
        else{
            if($this->DB->query("SELECT DISTINCT state_id FROM {$this->schema} WHERE state_id = ? LIMIT 1", array($state_id))->numRows() == 1){
                return true;
            } else{
                return false;
            }
        }

    }

    public function zipexists($zip){
        if(is_null($zip)){
            return false;
        } else{
            if($this->DB->query("SELECT DISTINCT zip FROM {$this->schema} WHERE zip = ? LIMIT 1", array($zip))->numRows() == 1){
                return true;
            } else{
                return false;
            }
        }
    }

    public function getStateInfo($state_id){
        $data = array();
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE state_id = ?", array($state_id));
        $data['zipcodeCount'] = $result->numRows();
        $resultData = $result->fetchAll();


        $data['state_name'] = $resultData[0]['state_name'];
        $data['AllData'] = $resultData;

        return $data;
    }

    public function getStateFromStateId($state_id){
        if(self:: validateState($state_id)){
            $result = $this->DB->query("SELECT state_name FROM {$this->schema} WHERE state_id = ? LIMIT 1", arrray($state_id))->fetchAll();
            return $result['state_name'];
        } else{
            return "Invalid";
        }
    }
}