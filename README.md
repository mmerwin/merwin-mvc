# merwin-MVC

Installation and setup:

A) Make sure to active the .htaccess files using the instructions found on http://merwincode.com/viewElements.php?ID=6 under "How to Activate an .htaccess file"

How to activate an .htaccess file on DigitalOcean Ubuntu Server

This is derived from the following post: https://www.digitalocean.com/community/questions/how-to-activate-an-htaccess-file

1) enable mod_rewrite using the command: sudo a2enmod rewrite

2) go to the /etc/apache2/sites-available/000-default.conf file

3) add the following to the bottom of the page

```
 <Directory /var/www/html/public>

 Options Indexes FollowSymLinks MultiViews
 AllowOverride All
 Order allow,deny
 allow from all
 </Directory>
```
In that same file, change the line 
```
DocumentRoot /var/www/html
```
to
```
DocumentRoot /var/www/html/public
```
This will change apache's default root directory for port 80. 

restart apache server using: sudo service apache2 restart


B) update the .htaccess file located in merwin-mvc/.htaccess to change " RewriteBase /merwin-mvc " to the directory you want the framework to be hosted inside.

C) change session_name() in app/core/App.php to a unrecognizable random string of letters and numbers. This helps keep the session data unique and unguessable.