<?php

class Service_ParticipantLogin
{
    public function loginParticipant($fname, $lname, $byear){
        $login = DB_Users::participantCredentialCheck($fname, $lname, $byear);
        $data = array();
        if($login->numRows() == 1){
            $data['valid'] = true;
            $loginData = $login->fetchArray();
            $data['username'] = $loginData['username'];
            $data['waiver'] = $loginData['waiver'];
            $data['claimed'] = $loginData['claimed'];


        } else{
            $data['valid'] = false;
        }

        return $data;
    }

}